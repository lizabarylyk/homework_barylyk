const modalWindowDeclarations = [
    {
      id: 'modalId1',
      header: 'Do you want to delete this file?',
      closeButton: true,
      text: 'Once you delete this file, it won’t be possible to undo this action. Are you sure you want to delete it?',
    },
    {
      id: 'modalId2',
      header: 'Do you want to delete this file?',
      closeButton: true,
      text: 'Once you delete this file, it won’t be possible to undo this action. Are you sure you want to delete it?',
    },
  ]
  export default modalWindowDeclarations
  