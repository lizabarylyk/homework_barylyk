let icons = document.querySelectorAll('.fas');
icons.forEach((item) => {
  item.addEventListener('click', selectIcon);
});

function selectIcon(event) {
  console.log(event.target);
  let target = event.target;
  target.classList.toggle('fa-eye-slash');
  if (target.classList.contains('fa-eye-slash')) {
    target.previousElementSibling.type = 'text';
  } else {
    target.previousElementSibling.type = 'password';
  }
}

let createDiv = document.createElement('span');
createDiv.innerHTML = 'Нужно ввести одинаковые значения';
createDiv.style.color = 'red';
let input1 = document.querySelector('.input-one');
let input2 = document.querySelector('.input-two');
let button = document.querySelector('.btn');

button.addEventListener('click', elem => {
  elem.preventDefault();
  if (
    input1.value === input2.value &&
    input1.value !== '' &&
    input2.value !== ''
  ) {
    createDiv.remove();
    alert('You are welcome');
    console.log(button);
  } else {
    button.after(createDiv);
  }
});
