import React, { Component } from 'react'
import Button from './components/Button/Button'
import Modal from './components/Modal/Modal'

export default class App extends Component {
  constructor(props) {
    super(props)
    this.state = {
      modalShow: false,
      modalInfo: {},
    }
  }

  showModal() {
    this.setState({
      modalShow: true,
    })
  }

  generateModal = (modalObj) => {
    this.showModal()
    this.setState({
      modalInfo: {
        ...modalObj,
        actions: (
          <>
            <button
              type="button"
              className="modal__content__btn"
              data-bs-dismiss="modal"
              onClick={this.closeModal}
            >
              Yes
            </button>
            <button
              type="button"
              className="modal__content__btn"
              data-bs-dismiss="modal"
              onClick={this.closeModal}
            >
              Cancel
            </button>
          </>
        ),
      },
    })
  }

  closeModal = () => {
    this.setState({ modalShow: false, modalContent: {} })
  }

  render() {
    const { modalShow, modalInfo } = this.state
    return (
      <>
        <Button
          title="Open first modal"
          backgroundColor="blue"
          modalID="modalId1"
          onClick={this.generateModal}
        />
        <Button
          title="Open second modal"
          backgroundColor="yellow"
          modalID="modalId2"
          onClick={this.generateModal}
        />
        {modalShow && <Modal modalObj={modalInfo} onClose={this.closeModal} />}
      </>
    )
  }
}
