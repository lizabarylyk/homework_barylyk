import axios from 'axios'
import { setProducts } from '../../../../homework_barylyk/react_homework_4/src/store/actions'

export const fetchProducts = (url) => {
  return (dispatch) => {
    axios.get(url).then(({ data }) => {
      dispatch(setProducts(data.products))
    })
  }
}
