$.fn.extend({
    toggleText: function (a, b) {
      return this.text(this.text() == b ? a : b);
    },
  });

  $('.header-left').after(
    $(`<ul class='section-url'>
  <li class='most-popular-url'><a href="#most-popular-posts">Most Popular</a></li>
  <li class='clients-url'><a href="#section-featured-clients">Clients</a></li>
  <li class='top-rated-url'><a href="#top-rated-block">Top Rated</a></li>
  <li class='hot-news-url'><a href="#hot-news">Hot News</a></li>
  </ul>`)
  );
  
  $('.section-url').css({
    display: 'flex',
    'align-items': 'center',
    
  });

  $('.section-url li').css({
    'font-family': 'Open Sans',
    'padding-right': '30px',
    'font-weight': '600',
    'font-size': '15px',
    'justify-content': 'space-between',
  
  });
  
  $('.top-rated').append($('<button>', { text: 'Свернуть', class: 'toggleButton' }));
  $('button').click(function () {
    $('.top-rated-images').slideToggle('slow');
    $('.toggleButton').toggleText('Свернуть', 'Развернуть');
  });

  $('a[href^="#"]').click(function () {
    let anchor = $(this).attr('href');
    $('html, body').animate(
      {
        scrollTop: $(anchor).offset().top,
      },
      1500
    );
  });

$(document).ready(function() { 
  let button = $('#button-up');  
  $(window).scroll (function () {
    if ($(this).scrollTop () > 300) {
      button.fadeIn();
    } else {
      button.fadeOut();
    }
});   
button.on('click', function(){
$('body, html').animate({
scrollTop: 0
}, 800);
return false;
});     
});
