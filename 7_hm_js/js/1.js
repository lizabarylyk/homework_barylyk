function list(arr, parent = 'body') {
  const list = document.createElement('ul');
  document.querySelector(parent).append(list);

  let resultArr = arr.map((elem) => {
    // const liElement = document.createElement('li');
    // list.insertAdjacentHTML('afterbegin', `<li>${elem}</li>`);
    // return (elem);
    return `<li>${elem}</li>`;
  });
  list.insertAdjacentHTML('afterbegin', resultArr)

  console.log(resultArr);
}

list(['1', '2', '3', 'sea', 'user', 23]);