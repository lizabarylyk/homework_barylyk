import React, { Component } from 'react'
import Star from '../../img/star.svg'
import ShoppingBag from '../../img/shopping_cart.png'
import './Header.scss'

export default class Header extends Component {
  render() {
    const { productsInCart, productsInFavourite } = this.props
    return (
      <div className="header">
        <div className="header__logo">
          <h1>Amazon</h1>
        </div>
        <div className="header__items">
          <div className="item__cart">
            <img
              src={ShoppingBag}
              alt="cart"
              width="40px"
            ></img>
            <p>{`: ${productsInCart}`}</p>
          </div>
          <div className="item__favorite">
            <img src={Star} alt="star" width="30px"></img>
            <p>{`: ${productsInFavourite}`}</p>
          </div>
        </div>
      </div>
    )
  }
}
