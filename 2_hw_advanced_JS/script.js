const books = [
    { 
      author: "Люсі Фолі",
      name: "Список запрошених",
      price: 70 
    }, 
    {
     author: "Сюзанна Кларк",
     name: "Джонатан Стрейндж і м-р Норрелл",
    }, 
    { 
      name: "Дизайн. Книга для недизайнерів.",
      price: 70
    }, 
    { 
      author: "Алан Мур",
      name: "Неономікон",
      price: 70
    }, 
    {
     author: "Террі Пратчетт",
     name: "Рухомі картинки",
     price: 40
    },
    {
     author: "Анґус Гайленд",
     name: "Коти в мистецтві",
    }
  ];


  const root = document.createElement('div');
  root.classList.add('root');
  document.body.appendChild(root);
  
  const ul = document.createElement('ul');
  root.appendChild(ul);
  
  let bookList = [];
  
  for (const book of books) {
    for (const key in book) {
      if (!bookList.includes(key)) {
        bookList.push(key);
      }
    }
  }
  
  for (const book of books) {
    try {
      for (const key of bookList) {
        if (book[key] === undefined) {
          throw new Error(`В книзі ${book.name} відсутнє поле ${key}`);
        }
      }
      const li = document.createElement('li');
      li.innerHTML = Object.entries(book)
        .map(([key, value]) => `${key}: ${value}`)
        .join(', ');
      ul.appendChild(li);
    } catch (error) {
      console.log(error);
    }
  }
  
  console.log(bookList);
  
