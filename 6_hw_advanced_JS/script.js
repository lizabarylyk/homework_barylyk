const button = document.createElement('button');
button.classList.add('.btn');
button.innerText = 'Вычислить по IP';
document.body.append(button);

button.addEventListener('click', e => {
    async function getIP() {
        let response = await fetch('https://api.ipify.org/?format=json');
        let ip = await response.json();

        let addressResponse = await fetch(`http://ip-api.com/json/${Object.values(ip)}?fields=continent,country,region,city,district`);
        let addressUser = await addressResponse.json();

        let user = document.querySelector('.user-address');
        user.innerHTML = `<p><span>Континент:</span> ${addressUser.continent}</p>
<p><span>Країна:</span> ${addressUser.country}</p>
 <p><span>Регіон:</span>  ${addressUser.region}</p>
<p><span>Місто:</span>  ${addressUser.city}</p>
<p><span>Район: </span> ${addressUser.district}</p>`
        document.body.append(user)
        return addressUser
    }

    getIP()
})

