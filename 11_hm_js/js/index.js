const tab = document.querySelectorAll('.btn');
function selectKey() {
  window.addEventListener('keydown', (event) => {
    const key = event.key;
    for (const btn of tab) {
      btn.classList.remove('active');
      if (btn.innerText === key) {
        btn.classList.add('active');
      }
    };
  });
}

selectKey();