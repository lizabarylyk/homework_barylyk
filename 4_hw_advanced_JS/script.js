'use strict';

 function request (url, queryParams) {
    const newQueryParams = new URLSearchParams(queryParams);
  
    return fetch(`${url}${queryParams? '?' + newQueryParams: ''}`)
    .then((response) => {
    return response.json();
  });
  }
  

  function getData() {
     request ('https://ajax.test-danit.com/api/swapi/films').then((dataInfo) => {
      const root = document.querySelector('body');

      dataInfo.forEach(({episodeId, name, openingCrawl, characters}) => {
        const filmInfo = document.createElement('div');

        filmInfo.insertAdjacentHTML(
          'beforeend',
          `<h2>Epizode ${episodeId}: ${name}</h2>
           <p>${openingCrawl}</p>`
        );
        // console.log(filmInfo);
    
        root.insertAdjacentElement('beforeend', filmInfo);
        getCharactersInfo(characters, filmInfo);

      });
    });
  }


  function getCharactersInfo (charactersInfo, filmInfo) {
    const listOfCharacters = document.createElement('ul');
    listOfCharacters.insertAdjacentText('afterbegin', 'Characters:');
  

    charactersInfo.forEach((url) => {

      request (url).then(({name}) => {
        listOfCharacters.insertAdjacentHTML('beforeend', `<li>${name}</li>`);
      });
  
      filmInfo.insertAdjacentElement('beforeend', listOfCharacters);
    });
  }

  
  getData();
