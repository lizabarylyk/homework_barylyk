import React, { Component } from 'react'
import './Button.scss'

import modalWindowDeclarations from '../../configs/modalWindowDeclarations'

export default class Button extends Component {
  clickHandler = (e) => {
    const modalID = e.target.dataset.modalId
    const modalInfoObject = modalWindowDeclarations.find(
      (modal) => modal.id === modalID
    )
    this.props.onClick(modalInfoObject)
  }

  render() {
    const { title, backgroundColor, modalID } = this.props
    return (
      <button
        data-modal-id={modalID}
        className={`btn ${backgroundColor}`}
        onClick={this.clickHandler}
      >
        {title}
      </button>
    )
  }
}
