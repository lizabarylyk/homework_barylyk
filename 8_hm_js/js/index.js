const bodyElem = document.body;
const divElem = document.createElement('div');
const inputElem = document.createElement('input');
inputElem.placeholder = 'Price';
inputElem.type = 'number';
bodyElem.prepend(divElem);
divElem.prepend(inputElem);
inputElem.addEventListener('focus', () => {
  inputElem.classList.add('success');
  inputElem.style.color = '';
});

inputElem.addEventListener('blur', () => {
  inputElem.classList.remove('success');
  isValid();
});

const failed = document.createElement('p');
function isValid() {
  if (+inputElem.value < 0) {
    inputElem.classList.add('fail');
    inputElem.classList.remove('success');
    failed.innerText = 'Please enter correct price.';
    inputElem.after(failed);
  } else {
    failed.remove();
    inputElem.classList.remove('fail');
    inputElem.style.color = 'green';
    const spanElem = document.createElement('span');
    spanElem.innerText = `Текущая цена: ${inputElem.value}`;
    spanElem.className = 'price';
    divElem.prepend(spanElem);
    const buttonElem = document.createElement('button');
    buttonElem.className = 'remove-button';
    buttonElem.innerText = 'x';
    spanElem.append(buttonElem);
    buttonElem.addEventListener('click', () => {
      spanElem.remove();
      inputElem.value = '';
    });
  }
}