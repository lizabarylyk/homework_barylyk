class Employee {
  constructor(name, age, salary) {
    this.name = name;
    this.age = age;
    this.salary = salary;
  }

  set name(name) {
    this._name = name;
  }

  get name() {
    return this._name;
  }

  set age(age) {
    this._age = age;
  }

  get age() {
    return this._age;
  }

  set salary(salary) {
    this._salary = salary;
  }

  get salary() {
    return this._salary;
  }
}

class Programmer extends Employee {
  constructor(name, age, salary, lang) {
    super(name, age, salary);
    this._lang = lang;
  }

  set salary(salary) {
    this._salary = salary * 3;
  }

  get salary() {
    return this._salary;
  }
}

const front = new Programmer("Louis", 28, 23000, "fr");
const back = new Programmer("Andriy", 35, 25000, "ukr");
const data = new Programmer("Ilon", 47, 29000, "eng");

console.log(front);
console.log(back);
console.log(data);
