let name = prompt("What is your name?");
let age = prompt("How old are you?");
console.log(age);

while (name === "" || isNaN(age)) {
  name = prompt("What is your name?", name);
  age = prompt("How old are you?", age);
  if (name === false || age === false) {
    break;
  }
}

if (age < 18) {
  alert("You are not allowed to visit this website");
}

// else if (age >= 18 && age <= 22)
else if (age <= 22) {
  if (confirm("Are you sure you want to continue?")) {
    alert("Welcome, " + name);
  } else {
    alert("You are not allowed to visit this website");
  }
}

// else if (age >= 23)
// {
// alert('Welcome, ' + name);
// }
else {
  alert("Welcome " + name);
}
