const image = document.querySelectorAll('.image-to-show');
const resetDisplay = () => {
  for (const img of image) {
    img.style.display = 'none';
    image[0].style.display = 'block';
  }
};
resetDisplay();

let count = 1;
function showImages() {
  if (count < 4) {
    image[count].style.display = 'block';
  }
  if (count > 0) {
    image[count - 1].style.display = 'none';
  }
  count++;
  if (count > 4) {
    resetDisplay();
    count = 1;
  }
}

let start = setInterval(showImages, 3000);
const cancel = document.createElement('button');
const resume = document.createElement('button');

cancel.innerText = 'Прекратить';
resume.innerText = 'Возобновить показ';

document.body.append(cancel);
document.body.append(resume);

cancel.addEventListener('click', () => {
  clearInterval(start);
});

resume.addEventListener('click', () => {
  start = setInterval(showImages, 3000);
});
