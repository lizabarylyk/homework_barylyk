const tabMenu = document.querySelector('.tabs');
console.log(tabMenu);
tabMenu.addEventListener('click', (event) => {
  if (event.target.tagName === 'LI') {
    change(event);
  }
});
const change = (event) => {
  let menuNav = document.querySelectorAll('.tabs-title');
  for (const item of menuNav) {
    if (item.classList.contains('active')) {
      item.classList.remove('active');
    }
  }
  if (!event.target.classList.contains('active')) {
    event.target.classList.add('active');
  }
  let id = event.target.innerHTML.toLowerCase();
  console.log(id);
  let content = document.querySelectorAll('.item');
  for (const item of content) {
    if (item.classList.contains('active')) {
      item.classList.remove('active');
    }
  }
  if (!document.getElementById(id).classList.contains('active')) {
    document.getElementById(id).classList.add('active');
  }
};