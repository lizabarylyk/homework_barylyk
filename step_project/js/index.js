// Секция "Our Services"

const serviceTab = document.querySelector('.tab-menu');
serviceTab.addEventListener('click', (event) => {
  if (event.target.tagName === 'BUTTON') {
    changeServices(event);
  }
});

const changeServices = (event) => {
  let menuItem = document.querySelectorAll('.tabs-title');
  for (const item of menuItem) {
    if (item.classList.contains('active')) {
      item.classList.remove('active');
    }
  }
  if (!event.target.classList.contains('active')) {
    event.target.classList.add('active');
  }
  let id = event.target.innerText.replace(/ /, '-').toLowerCase();
  let content = document.querySelectorAll('.disc');
  for (const item of content) {
    if (item.classList.contains('active')) {
      item.classList.remove('active');
    }
  }
  if (!document.getElementById(id).classList.contains('active')) {
    document.getElementById(id).classList.add('active');
  }
};

// Секция "Our Amazing Work"

const ourWorkTab = document.querySelector('.work-menu-wrap');
ourWorkTab.addEventListener('click', (event) => {
  if (event.target.tagName === 'BUTTON') {
    changeOurWork(event);
  }
});

const changeOurWork = (event) => {
  let workContent = document.querySelector('.work-content');
  let contentItems = document.querySelectorAll('.work-content .item');
  let className = event.target.innerText.replace(/ /, '_').toLowerCase();
  for (const item of contentItems) {
    if (!item.classList.contains(`${className}`)) {
      item.classList.remove('active');
      workContent.style.width = '900px';
    }
    if (item.classList.contains(`${className}`)) {
      item.classList.add('active');
    }
    if (className === 'all') {
      item.classList.add('active');
      workContent.style.width = '';
    }
  }
  let tabMenu = document.querySelectorAll('.work-menu-wrap>button');
  for (const item of tabMenu) {
    if (item.classList.contains('active')) {
      item.classList.remove('active');
    }
  }
  if (!event.target.classList.contains('active')) {
    event.target.classList.add('active');
  }
};

const loadMore = document.querySelector('.button-load-wrap');
loadMore.addEventListener('click', () => {
  let activeBtn = document.querySelector('#all');
  activeBtn.click();
  let workContent = document.querySelector('.work-content');
  workContent.insertAdjacentHTML(
    'beforeend',
    `<div class="item web_design active">
 <div class="hover-menu">
 <div class="hover-buttons">
   <button class="small-button1">
     <img src="./images/Combined-shape-7431.svg" alt="">
   </button>
   <button class="small-button2">
     <div class="cube">
     </div>
   </button>
 </div>
 <p class="hover-title">Creative design</p>
 <p class="hover-text">Web Design</p>
 </div>
<img src="./images/work/Layer 24.png" alt="">
</div>

<div class="item graphic_design active">
 <div class="hover-menu">
   <div class="hover-buttons">
     <button class="small-button1">
         <img src="./images/Combined-shape-7431.svg" alt="">
       </button>
       <button class="small-button2">
         <div class="cube">
       </div>
     </button>
   </div>
   <p class="hover-title">Creative design</p>
   <p class="hover-text">Web Design</p>
   </div>
<img src="./images/work/Layer 25.png" alt="">
</div>

<div class="item graphic_design active">
 <div class="hover-menu">
   <div class="hover-buttons">
     <button class="small-button1">
         <img src="./images/Combined-shape-7431.svg" alt="">
       </button>
       <button class="small-button2">
         <div class="cube">
       </div>
     </button>
   </div>
   <p class="hover-title">Creative design</p>
   <p class="hover-text">Web Design</p>
   </div>
<img src="./images/work/Layer 26.png" alt="">
</div>

<div class="item landing_pages active">
 <div class="hover-menu">
   <div class="hover-buttons">
     <button class="small-button1">
         <img src="./images/Combined-shape-7431.svg" alt="">
       </button>
       <button class="small-button2">
         <div class="cube">
       </div>
     </button>
   </div>
   <p class="hover-title">Creative design</p>
   <p class="hover-text">Web Design</p>
   </div>
<img src="./images/work/Layer 27.png" alt="">
</div>

<div class="item wordpress active">
 <div class="hover-menu">
   <div class="hover-buttons">
     <button class="small-button1">
         <img src="./images/Combined-shape-7431.svg" alt="">
       </button>
       <button class="small-button2">
         <div class="cube"> 
       </div>
     </button>
   </div>
   <p class="hover-title">Creative design</p>
   <p class="hover-text">Web Design</p>
   </div>
<img src="./images/work/Layer 28.png" alt="">
</div>

<div class="item graphic_design active">
 <div class="hover-menu">
   <div class="hover-buttons">
     <button class="small-button1">
         <img src="./images/Combined-shape-7431.svg" alt="">
       </button>
       <button class="small-button2">
         <div class="cube">
       </div>
     </button>
   </div>
   <p class="hover-title">Creative design</p>
   <p class="hover-text">Web Design</p>
   </div>
<img src="./images/work/Layer 29.png" alt="">
</div>

<div class="item web_design active">
 <div class="hover-menu">
   <div class="hover-buttons">
     <button class="small-button1">
         <img src="./images/Combined-shape-7431.svg" alt="">
       </button>
       <button class="small-button2">
         <div class="cube">
       </div>
     </button>
   </div>
   <p class="hover-title">Creative design</p>
   <p class="hover-text">Web Design</p>
   </div>
<img src="./images/work/Layer 30.png" alt="">
</div>

<div class="item landing_pages active">
 <div class="hover-menu">
   <div class="hover-buttons">
     <button class="small-button1">
         <img src="./images/Combined-shape-7431.svg" alt="">
       </button>
       <button class="small-button2">
         <div class="cube">   
       </div>
     </button>
   </div>
   <p class="hover-title">Creative design</p>
   <p class="hover-text">Web Design</p>
   </div>
<img src="./images/work/Layer 31.png" alt="">
</div>

<div class="item wordpress active">
 <div class="hover-menu">
   <div class="hover-buttons">
     <button class="small-button1">
         <img src="./images/Combined-shape-7431.svg" alt="">
       </button>
       <button class="small-button2">
         <div class="cube">
       </div>
     </button>
   </div>
   <p class="hover-title">Creative design</p>
   <p class="hover-text">Web Design</p>
   </div>
<img src="./images/work/Layer 32.png" alt="">
</div>

<div class="item wordpress active">
 <div class="hover-menu">
   <div class="hover-buttons">
     <button class="small-button1">
         <img src="./images/Combined-shape-7431.svg" alt="">
       </button>
       <button class="small-button2">
         <div class="cube">
       </div>
     </button>
   </div>
   <p class="hover-title">Creative design</p>
   <p class="hover-text">Web Design</p>
   </div>
<img src="./images/work/Layer 33.png" alt="">
</div>

<div class="item web_design active">
 <div class="hover-menu">
   <div class="hover-buttons">
     <button class="small-button1">
         <img src="./images/Combined-shape-7431.svg" alt="">
       </button>
       <button class="small-button2">
         <div class="cube">  
       </div>
     </button>
   </div>
   <p class="hover-title">Creative design</p>
   <p class="hover-text">Web Design</p>
   </div>
<img src="./images/work/Layer 34.png" alt="">
</div>

<div class="item landing_pages active">
 <div class="hover-menu">
   <div class="hover-buttons">
     <button class="small-button1">
         <img src="./images/Combined-shape-7431.svg" alt="">
       </button>
       <button class="small-button2">
         <div class="cube">        
       </div>
     </button>
   </div>
   <p class="hover-title">Creative design</p>
   <p class="hover-text">Web Design</p>
   </div>
<img src="./images/work/Layer 24.png" alt="">
</div>`
  );
  let contentItems = document.querySelectorAll('.work-content .item');
  for (const item of contentItems) {
    console.log(item.classList.contains('active'));
  }
  loadMore.remove();
});


// Секция "What people say"

const clientTab = document.querySelector('.swiper-images');
clientTab.addEventListener('click', (event) => {
  if (event.target.tagName === 'IMG') {
    changeClients(event);
  }
});

const changeClients = (event) => {
  let clientItems = document.querySelectorAll('.swiper-images>img');
  for (const item of clientItems) {
    if (item.classList.contains('active')) {
      item.classList.remove('active');
    }
  }
  if (!event.target.classList.contains('active')) {
    event.target.classList.add('active');
  }
  let id = event.target.classList[0];
  let contentItems = document.querySelectorAll('.disc-people');
  for (const item of contentItems) {
    if (item.classList.contains('active')) {
      item.classList.remove('active');
    }
  }
  if (!document.getElementById(id).classList.contains('active')) {
    document.getElementById(id).classList.add('active');
  }
};

const nextBtn = document.querySelector('.next');
nextBtn.addEventListener('click', () => {
  let clientItems = document.querySelectorAll('.swiper-images>img');
  if (clientItems[0].classList.contains('active')) {
    let activeContent = document.querySelector('#anna');
    let newActiveContent = document.querySelector('#artem');
    activeContent.classList.remove(`${clientItems[0].classList[1]}`);
    newActiveContent.classList.add(`${clientItems[0].classList[1]}`);
    clientItems[1].classList.add('active');
    clientItems[0].classList.remove('active');
  } else if (clientItems[1].classList.contains('active')) {
    let activeContent = document.querySelector('#artem');
    let newActiveContent = document.querySelector('#hasan');
    activeContent.classList.remove(`${clientItems[1].classList[1]}`);
    newActiveContent.classList.add(`${clientItems[1].classList[1]}`);
    clientItems[2].classList.add('active');
    clientItems[1].classList.remove('active');
  } else if (clientItems[2].classList.contains('active')) {
    let activeContent = document.querySelector('#hasan');
    let newActiveContent = document.querySelector('#olga');
    activeContent.classList.remove(`${clientItems[2].classList[1]}`);
    newActiveContent.classList.add(`${clientItems[2].classList[1]}`);
    clientItems[3].classList.add('active');
    clientItems[2].classList.remove('active');
  } else if (clientItems[3].classList.contains('active')) {
    let activeContent = document.querySelector('#olga');
    let newActiveContent = document.querySelector('#anna');
    activeContent.classList.remove(`${clientItems[3].classList[1]}`);
    newActiveContent.classList.add(`${clientItems[3].classList[1]}`);
    clientItems[0].classList.add('active');
    clientItems[3].classList.remove('active');
  }
});

const prevBtn = document.querySelector('.previous');
prevBtn.addEventListener('click', () => {
  let clientItems = document.querySelectorAll('.swiper-images>img');
  if (clientItems[0].classList.contains('active')) {
    let activeContent = document.querySelector('#anna');
    let newActiveContent = document.querySelector('#olga');
    activeContent.classList.remove(`${clientItems[0].classList[1]}`);
    newActiveContent.classList.add(`${clientItems[0].classList[1]}`);
    clientItems[3].classList.add('active');
    clientItems[0].classList.remove('active');
  } else if (clientItems[1].classList.contains('active')) {
    let activeContent = document.querySelector('#artem');
    let newActiveContent = document.querySelector('#anna');
    activeContent.classList.remove(`${clientItems[1].classList[1]}`);
    newActiveContent.classList.add(`${clientItems[1].classList[1]}`);
    clientItems[0].classList.add('active');
    clientItems[1].classList.remove('active');
  } else if (clientItems[2].classList.contains('active')) {
    let activeContent = document.querySelector('#hasan');
    let newActiveContent = document.querySelector('#artem');
    activeContent.classList.remove(`${clientItems[2].classList[1]}`);
    newActiveContent.classList.add(`${clientItems[2].classList[1]}`);
    clientItems[1].classList.add('active');
    clientItems[2].classList.remove('active');
  } else if (clientItems[3].classList.contains('active')) {
    let activeContent = document.querySelector('#olga');
    let newActiveContent = document.querySelector('#hasan');
    activeContent.classList.remove(`${clientItems[3].classList[1]}`);
    newActiveContent.classList.add(`${clientItems[3].classList[1]}`);
    clientItems[2].classList.add('active');
    clientItems[3].classList.remove('active');
  }
});
